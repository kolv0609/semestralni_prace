package cz.vse.kolv;

import cz.vse.kolv.logika.*;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.*;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

public class MainController {
    public Label locationName;
    public Label locationDescription;
    public TextField textInput;
    public TextArea textOutput;
    public ImageView pozadi;
    private IHra hra;
    public VBox items;
    public VBox exits;
    public VBox backpack;
    public Button closeButton;
    public Button gameButton;

    public void init(IHra hra){
        this.hra = hra;
        update();
    }

    /**
     * Metoda ukládající do proměnné typu String název aktuálního prostoru ve hře.
     * Dále aktualizuje vypisující se text a pozadí (obrázky) v závislosti na aktuálním prostoru.
     *
     */
    private void update(){
        String location = getAktualniProstor().getNazev();
        locationName.setText(location);
        String description = getAktualniProstor().popis();
        locationDescription.setText(description);

        InputStream stream = getClass().getClassLoader().getResourceAsStream(location + ".jpg");
        Image img = new Image(stream);
        pozadi.setImage(img);
        pozadi.setFitWidth(1600);
        pozadi.setFitHeight(1000);

        updateExits();
        updateItems();
        updateBackpack();
        konec();
    }

    /**
     * V této metodě se aktualizují předměty v batohu včetně jejich obrázkové podoby.
     * Je zde nastavena akce, která se spustí kliknutím myši na daný předmět.
     * Jedná se o příkaz, který provede vyndání předmětu z batohu.
     */
    private void updateBackpack() {
        Collection<Vec> backpackList = getSeznamVeci().getVeciVBatohu().values();
        backpack.getChildren().clear();

        for (Vec backpackItem : backpackList){
            String backpackItemName = backpackItem.getNazev();
            Label backpackItemLabel = new Label(backpackItemName);
            backpackItemLabel.getStyleClass().add("Hover");

            setImages(backpackItemName, backpackItemLabel);

            backpackItemLabel.setCursor(Cursor.HAND);
            backpackItemLabel.setOnMouseClicked(event -> {
                String result = hra.spustPrikaz("poloz " + backpackItemName);
                hra.getHerniPlan().getBatoh().odebrat(backpackItemName);

                textOutput.appendText(result + "\n\n");
                update();
            });

            backpack.getChildren().add(backpackItemLabel);
        }
    }

    /**
     * Metoda nastavující grafické pozadí pro danou místnost.
     * Pozadí je nastaveno dle parametrů, na základě kterých se vybere správný obrázek.
     *
     * @param backpackItemName
     * @param backpackItemLabel
     */
    private void setImages(String backpackItemName, Label backpackItemLabel) {
        InputStream stream = getClass().getClassLoader().getResourceAsStream(backpackItemName + ".jpg");
        Image img = new Image(stream);
        ImageView imageView = new ImageView(img);
        imageView.setFitWidth(60);
        imageView.setFitHeight(40);
        backpackItemLabel.setGraphic(imageView);
    }

    /**
     * Touto metodou se aktualizují předměty nacházející se v aktuální místnosti.
     * Zároveň se zobrazují jejich obrázkové podoby.
     * Pří kliknutí myši na daný předmět se provede příkaz vkládající
     * předmět do batohu.
     * Při najetí kurzorem myší na předmět se zobrazí popisek o daném předmětu.
     */
    private void updateItems() {
        Collection<Vec> itemList = getAktualniProstor().getVeci().values();
        items.getChildren().clear();

        for (Vec item : itemList) {
            String itemName = item.getNazev();
            Label itemLabel = new Label(itemName);

            setImages(itemName, itemLabel);

            if(item.lzeSebrat()){
                itemLabel.setCursor(Cursor.HAND);
                itemLabel.getStyleClass().add("Hover");
                itemLabel.setTooltip(new Tooltip("Tato věc lze sebrat, případně nějak použít."));

                itemLabel.setOnMouseClicked(event -> {
                    zpracujPrikaz("seber " + itemName);
                });
            } else if(item.jdeOdemknout()){
                itemLabel.setTooltip(new Tooltip("Tato věc nelze sebrat, ale lze odemknout."));
            }
            else if(item.jdeRozriznout()){
                itemLabel.setTooltip(new Tooltip("Tato věc nelze sebrat, ale lze rozříznout."));
            }
            else if(item.lzeSnist()){
                itemLabel.setTooltip(new Tooltip("Tato věc lze sebrat a sníst."));
            }
            else{
                itemLabel.setTooltip(new Tooltip("Tato věc nelze sebrat."));

            }
            items.getChildren().add(itemLabel);
        }
    }

    /**
     * Metoda aktualizující seznam dostupných východů z aktuální místnosti
     * včetně jejich obrázkových miniatur.
     * Při kliknutí myši na danou lokalitu se hráč přesouvá do zvolené místnosti.
     */
    private void updateExits() {
        Collection<Mistnost> exitList = getAktualniProstor().getVychody();
        exits.getChildren().clear();

        for (Mistnost mistnost : exitList){
            String exitName = mistnost.getNazev();
            Label exitLabel = new Label(exitName);

            exitLabel.setCursor(Cursor.HAND);

            setImages(exitName, exitLabel);
            exitLabel.getStyleClass().add("Hover");

            exitLabel.setTooltip(new Tooltip(mistnost.popis()));

            exitLabel.setOnMouseClicked(event -> {
                zpracujPrikaz("jdi " + exitName);
            });

            exits.getChildren().add(exitLabel);
        }
    }

    /**
     * Metoda pro zpracování příkazu.
     * Parametr určuje textový název příkazu.
     *
     * @param prikaz
     */
    private void zpracujPrikaz(String prikaz) {
        String result = hra.spustPrikaz(prikaz);
        textOutput.appendText(result + "\n\n");
        update();
    }

    /**
     * Metoda pro zjištění aktuální místnosti.
     * @return
     */
    private Mistnost getAktualniProstor() {
        return hra.getHerniPlan().getAktualniMistnost();
    }

    /**
     * Metoda vracející údaje o batohu.
     * @return
     */
    private Batoh getSeznamVeci(){
        return hra.getHerniPlan().getBatoh();
    }

    /**
     * Nastavení keyEventu - po stisknutí klávesy ENTER se provede
     * příkaz, který hráč zadal před stiskem klávesy ENTER.
     * @param keyEvent
     */
    public void onInputKeyPressed(KeyEvent keyEvent) {
        if(keyEvent.getCode() == KeyCode.ENTER){
            zpracujPrikaz(textInput.getText());
            textInput.setText("");
        }
    }

    /**
     * Metoda otevře nové okno obsahující textové informace ke hře.
     * Spouští se tlačítkem v menu.
     */
    public void actionAbout() {
        Stage secondStage = new Stage();
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.load(getClass().getResource("/textace.html").toString());
        Scene scene = new Scene(webView,600,600);
        secondStage.setScene(scene);
        secondStage.setTitle("O hře");
        secondStage.show();
    }

    /**
     * Po stisknutí tlačítka v menu se spouští tato metoda,
     * která otevře obrázek mapy hrací plochy.
     */
    public void actionMap() {
        Stage secondStage = new Stage();
        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();
        webEngine.load(getClass().getResource("/mapa.jpg").toString());
        Scene scene = new Scene(webView,600,600);
        secondStage.setScene(scene);
        secondStage.setTitle("Mapa");
        secondStage.show();
    }

    /**
     * Metoda k provedení příkazu odemkni. Je možné ji zadat z rozbalovacího menu.
     */
    public void odemkni() {
        String misto = getAktualniProstor().getNazev();

        if(misto == "Pracovna" && getSeznamVeci().obsahujeVec("klic")){
           String prikaz = "odemkni trezor";
           String result = hra.spustPrikaz(prikaz);
           textOutput.appendText(result + "\n");
           hra.spustPrikaz("seber hodinky");
           update();
        }
        else if(misto == "pracovna" && !(getSeznamVeci().obsahujeVec("klic"))){
            textOutput.appendText("V batohu nemáš klíč k otevření trezoru. \n");
            update();
        }
        else if(misto != "Pracovna" && getSeznamVeci().obsahujeVec("klic"))
        {
            textOutput.appendText("V batohu máš klíč, ale nenacházíš se v pracovně. \n");
            update();
        }
        else{
            textOutput.appendText("Musíš nejprve najít klíč a trezor k provedení tohoto příkazu. \n");
            update();
        }
    }

    /**
     * Metoda k provedení příkazu rozrizni. Je součástí rozbalovacího menu.
     */
    public void rozrizni() {
        String misto = getAktualniProstor().getNazev();
        if(misto == "detsky_pokoj" && getSeznamVeci().obsahujeVec("nuz")){
            String prikaz = "rozrizni medvidek";
            String result = hra.spustPrikaz(prikaz);
            hra.spustPrikaz("seber bonbon");
            textOutput.appendText(result + "\n");
            update();
        }
        else if(misto != "detsky_pokoj" && getSeznamVeci().obsahujeVec("nuz")){
            textOutput.appendText("Musíš nejprve najít plyšového medvídka. \n");
            update();
        }
        else{
            textOutput.appendText("Musíš nejprve najít nůž a plyšového medvídka k provedení příkazu. \n");
        }
    }

    /**
     * Metoda spustí příkaz sněz. Příkaz je možné spustit z rozbalovacího menu.
     */
    public void snez() {
        if(getSeznamVeci().obsahujeVec("bonbon")){
            String prikaz = "snez bonbon";
            String result = hra.spustPrikaz(prikaz);
            textOutput.appendText(result + "\n");
            update();
        }
        else{
            textOutput.appendText("Tvůj batoh neobsahuje nic, co by se dalo sníst. \n");
        }
    }

    /**
     * Metoda spustí příkaz napoveda. Příkaz je možné spustit z rozbalovacího menu.
     * Vypíše stručnou nápovědu pro postup ve hře.
     */
    public void napoveda() {
        String result = hra.spustPrikaz("napoveda");
        textOutput.appendText(result + "\n\n");
        update();
    }

    /**
     * Metoda zastaví hru v případě, že hráč buď vyhraje, případně ji ukončí příkazem.
     */
    public void konec() {
        if(hra.konecHry() == true){
            textInput.setEditable(false);
            hra = null;
        }
    }

    /**
     * Meota ukončí aplikaci po stisknutí tlačítka Konec hry.
     * @param actionEvent
     */
    @FXML
    public void handleCloseButtonAction(ActionEvent actionEvent) {
        Stage stage = (Stage) closeButton.getScene().getWindow();
        stage.close();
    }

    /**
     * Metoda spustí novou hru po stisknutí tlačítka.
     * @param actionEvent
     */
    @FXML
    public void handleNewGameButtonAction(ActionEvent actionEvent) {
        Stage stage = (Stage) gameButton.getScene().getWindow();
        stage.close();

        Stage primaryStage = new Stage();

        primaryStage.setFullScreen(true);
        primaryStage.setTitle("Hra");

        FXMLLoader loader = new FXMLLoader();
        InputStream stream = getClass().getClassLoader().getResourceAsStream("scene.fxml");
        Parent root = null;
        try {
            root = loader.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        MainController controller = loader.getController();
        IHra hra = new Hra();
        controller.init(hra);
        primaryStage.show();
    }
}
