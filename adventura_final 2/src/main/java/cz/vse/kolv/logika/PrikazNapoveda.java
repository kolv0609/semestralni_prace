
package cz.vse.kolv.logika;


/*******************************************************************************
 * Třída PrikazNapoveda implementuje příkaz nápověda.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class PrikazNapoveda implements IPrikaz
{
    private static final String nazev = "napoveda";
    private SeznamPrikazu platne;
    
    /**
     * Vytvoření příkazu.
     * 
     * @param platne - platné příkazy ve hře
     */
    public PrikazNapoveda(SeznamPrikazu platne)
    {
        this.platne = platne;
    }

    /**
     * Metoda spouští příkaz ve hře. Vrací text, který popisuje hru a úkoly v ní.
     * 
     * @return řetězec a seznam příkazů, které lze ve hře použít.
     */
    @Override
    public String spustPrikaz(String... parametry)
    {
        return "Cílem je sesbírat všechny dárky, které Santa Claus rozmístil po domě.\n"
        + "Mezi dárky patří: mobilní telefon, hodinky, parfém\n" +
        "Dále také musíš sníst bonbon, který je ukrytý v plyšovém medvídkovi.\n" +
        "Lze použít tyto příkazy:\n" + platne.nazvyPrikazu();
    }
    
    /**
     * Vrací název příkazu.
     * 
     * @return nazev příkazu
     */
    @Override
    public String getNazev()
    {
        return nazev;
    }
}
