
package cz.vse.kolv.logika;

import java.util.*;

/*******************************************************************************
 * Třída SeznamPrikazu, zaznamenává příkazy, které lze ve hře použít. Rozpoznává jednotlivé příkazy
 * a vrátí odkaz na třídu implementující konkrétní příkaz.
 *
 * @author  Vít Kollarczyk
 * @version 1.0
 */
public class SeznamPrikazu
{
    private Map<String,IPrikaz> mapaPrikazu;
    
    /**
     * Konstruktor pro vytvoření nové mapy příkazů.
     */
    public SeznamPrikazu()
    {
        mapaPrikazu = new HashMap<>();
    }
    
    /**
     * Přidává nový příkaz.
     * 
     * @param prikaz instance třídy implementující rozhraní IPrikaz.
     */
    public void pridejPrikaz(IPrikaz prikaz)
    {
        mapaPrikazu.put(prikaz.getNazev(),prikaz);
    }
    
    /**
     * Vrátí odkaz na instanci třídy, implementující rozhraní IPrikaz, která příkaz provede.
     * 
     * @param text - text příkazu, který hráč zavolá. 
     * @return - instance třídy, která provede daný příkaz.
     */
    public IPrikaz vratPrikaz(String text)
    {
        if(mapaPrikazu.containsKey(text))
        {
            return mapaPrikazu.get(text);
        }
        else
        {
            return null;
        }
    }
    
    /**
     * Ověřuje, zda-li je příkaz, který hráč zadá, platný a lze jej použít.
     * 
     * @param text - text příkazu, u kterého se ověřuje platnost.
     * @return true, pokud zadaný příkaz je mezi platnými příkazy ve hře, jinak false.
     */
    public boolean platnyPrikaz(String text)
    {
        return mapaPrikazu.containsKey(text);
    }
    
    /**
     * Vrací seznam dostupných příkazů. 
     * 
     * @return seznam - řetězec obsahující seznam dostupných příkazů ve hře.
     */
    public String nazvyPrikazu()
    {
        String seznam = "";
        for(String textPrikazu : mapaPrikazu.keySet())
        {
            seznam += textPrikazu + " ";
        }
        return seznam;
    }    
}
