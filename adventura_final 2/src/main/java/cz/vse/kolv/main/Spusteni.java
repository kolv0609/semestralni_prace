
package cz.vse.kolv.main;

import cz.vse.kolv.MainController;
import cz.vse.kolv.logika.*;
import cz.vse.kolv.uiText.TextoveRozhrani;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


import java.io.InputStream;
import java.util.Arrays;
import java.util.List;


/*******************************************************************************
 * Instance třídy {@code Spusteni} představují ...
 * The {@code Spusteni} class instances represent ...
 *
 * @author  author name
 * @version 0.00.0000 — 20yy-mm-dd
 */
public class Spusteni extends Application
{
    
    /***************************************************************************
     */
    public static void main(String[] args)
    {
        List<String> vstup = Arrays.asList(args);

        if(vstup.contains("text")) {
            IHra hra = new Hra();
            TextoveRozhrani ui = new TextoveRozhrani(hra);
            ui.hraj();
        } else {
            launch();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setFullScreen(true);
        primaryStage.setTitle("Hra");

        FXMLLoader loader = new FXMLLoader();
        InputStream stream = getClass().getClassLoader().getResourceAsStream("scene.fxml");
        Parent root = loader.load(stream);

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);

        MainController controller = loader.getController();
        IHra hra = new Hra();
        controller.init(hra);
        primaryStage.show();
    }
}