
package cz.vse.kolv;

import cz.vse.kolv.logika.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


import static org.junit.Assert.*;

/*******************************************************************************
 * Testovací třída HraTest slouží ke komplexnímu otestování
 * třídy Hra.
 *
 * @author  Vít Kollarczyk
 * @version 1.0 
 */
public class HraTest
{
    private Hra hra;
    
    /***************************************************************************
     * Metoda se provede před spuštěním testovací metody. Vytvoří se datové 
     * atributy, se kterými budou testovací metody pracovat.
     */
    @Before
    public void setUp()
    {
        hra = new Hra();
    }

    /***************************************************************************
     * Úklid po testu - tato metoda se spustí po vykonání každého testu.
     */
    @After
    public void tearDown()
    {
    }

    /***************************************************************************
     * Testuje průběh hry. Po zavolání každého příkazu se otestuje, v jaké místnosti
     * se právě hráč nachází a zda-li hra skončí.
     */
    @Test
    public void testPrubeh()
    {
               
        assertEquals("predsin", hra.getHerniPlan().getAktualniMistnost().getNazev());
        assertTrue(hra.getHerniPlan().getAktualniMistnost().nachaziSeVMistnosti("boty"));
        hra.spustPrikaz("seber boty");
        assertTrue(hra.getHerniPlan().getAktualniMistnost().nachaziSeVMistnosti("boty"));
        assertEquals(false, hra.konecHry());
        
        hra.spustPrikaz("jdi chodba");
        assertEquals("chodba",hra.getHerniPlan().getAktualniMistnost().getNazev());
        assertEquals(false, hra.konecHry());
        assertTrue(hra.getHerniPlan().getAktualniMistnost().nachaziSeVMistnosti("telefon"));
        hra.spustPrikaz("seber telefon");
        assertFalse(hra.getHerniPlan().getAktualniMistnost().nachaziSeVMistnosti("telefon"));
        assertEquals(false, hra.konecHry());
               
        hra.spustPrikaz("jdi Ložnice");
        assertEquals("Ložnice", hra.getHerniPlan().getAktualniMistnost().getNazev());
        assertEquals(false, hra.konecHry());
        assertTrue(hra.getHerniPlan().getAktualniMistnost().nachaziSeVMistnosti("klic"));
        hra.spustPrikaz("seber klic");
        assertFalse(hra.getHerniPlan().getAktualniMistnost().nachaziSeVMistnosti("klic"));
        assertEquals(false, hra.konecHry());
        
        hra.spustPrikaz("jdi chodba");
        assertEquals("chodba",hra.getHerniPlan().getAktualniMistnost().getNazev());
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("jdi obyvaci_pokoj");
        assertEquals("obyvaci_pokoj", hra.getHerniPlan().getAktualniMistnost().getNazev());
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("jdi Pracovna");
        assertEquals("Pracovna", hra.getHerniPlan().getAktualniMistnost().getNazev());
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("odemkni trezor");
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("poloz klic");
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("seber hodinky");
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("jdi obyvaci_pokoj");
        assertEquals("obyvaci_pokoj", hra.getHerniPlan().getAktualniMistnost().getNazev());
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("jdi kuchyn");
        assertEquals("kuchyn", hra.getHerniPlan().getAktualniMistnost().getNazev());
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("seber nuz");
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("jdi obyvaci_pokoj");
        assertEquals("obyvaci_pokoj", hra.getHerniPlan().getAktualniMistnost().getNazev());
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("jdi chodba");
        assertEquals("chodba", hra.getHerniPlan().getAktualniMistnost().getNazev());
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("jdi detsky_pokoj");
        assertEquals("detsky_pokoj", hra.getHerniPlan().getAktualniMistnost().getNazev());
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("rozrizni medvidek");
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("poloz nuz");
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("seber bonbon");
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("snez bonbon");
        assertEquals(false, hra.konecHry());
        hra.spustPrikaz("jdi chodba");
        assertEquals("chodba", hra.getHerniPlan().getAktualniMistnost().getNazev());
        assertEquals(false, hra.konecHry());
                      
        hra.spustPrikaz("jdi Koupelna");
        assertEquals("Koupelna",hra.getHerniPlan().getAktualniMistnost().getNazev());
        assertEquals(false, hra.konecHry());
        assertTrue(hra.getHerniPlan().getAktualniMistnost().nachaziSeVMistnosti("parfem"));
        hra.spustPrikaz("seber parfem");
        assertFalse(hra.getHerniPlan().getAktualniMistnost().nachaziSeVMistnosti("parfem"));
        assertEquals(true, hra.konecHry());
        
    }

}
